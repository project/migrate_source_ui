Migrate source UI

Visit /admin/config/content/migrate_source_ui to configure the Migrate Source UI module.

Visit /admin/content/migrate_source_ui to upload source files and run migrations.
